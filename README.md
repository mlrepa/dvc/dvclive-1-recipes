# DVCLive Examples and Recipes

This example shows the how to use [DVCLive](https://dvc.org/doc/dvclive)


![DVCLive Examples and Recipes](static/banner.png)

--------
Repository Structure

    ├── README.md          <- The top-level README for developers using this project.
    ├── config             <- Configs directory
    ├── data               <- Datasets
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    ├── notebooks          <- Example Jupyter Notebook
    └── static             <- Assets for docs 
     

## :woman_technologist: Installation

### 1. Fork / Clone this repository

Get the tutorial example code:

```bash
git clone git@gitlab.com:mlrepa/dvc/dvclive-1-recipes.git
cd dvclive-1-recipes.git
```


### 2. Create a virtual environment

- This example requires Python 3.9 or above 

```bash
python3 -m venv .venv
echo "export PYTHONPATH=$PWD" >> .venv/bin/activate
source .venv/bin/activate
pip install --upgrade pip setuptools wheel
pip install -r requirements.txt
```

## 🎓 Run the tutorial
```bash
jupyter lab
``` 
